//
//  AppDelegate+Injection.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation
import Resolver

extension Resolver: ResolverRegistering {
    
    public static func registerAllServices() {
        
        register {
            CredentialsProvider.default
        }
        .implements(CredentialsProviderProtocol.self)
        .scope(.graph)
        
        register {
            APIClient(
                provider: .default,
                credentialsProvider: resolve()
            )
        }
        .implements(APIClientProtocol.self)
        .scope(.graph)
        
        register {
            JSONDecoder()
        }
        .implements(DecoderProtocol.self)
        .scope(.shared)
        
        register {
            HomeVideosRepository(
                apiClient: resolve(),
                decoder: resolve()
            )
        }
        .implements(VideosRepositoryProtocol.self, name: "home")
        .scope(.graph)
        
        register {
            HomeLocalVideosDataRepository()
        }
        .implements(LocalVideosDataRepositoryProtocol.self, name: "home")
        .scope(.graph)
        
        register {
            GetHomeVideosUseCase(
                remoteRepository: resolve(
                    VideosRepositoryProtocol.self,
                    name: "home"
                ),
                localRepository: resolve(
                    LocalVideosDataRepositoryProtocol.self,
                    name: "home"
                )
            )
        }
        .implements(GetVideosUseCaseProtocol.self, name: "home")
        .scope(.graph)
        
        register {
            VideoDownloader(apiClient: resolve())
        }
        .implements(VideoDownloaderProtocol.self)
        .scope(.graph)
       
        register {
            DownloadVideosUseCase(downloader: resolve())
        }
        .implements(DownloadVideosUseCaseProtocol.self)
        .scope(.graph)
        
        register {
            AssetLoader()
        }
        .implements(AssetLoaderProtocol.self)
        .scope(.shared)
        
        register {
            HomeViewModel(
                getVideosUseCase: resolve(GetVideosUseCaseProtocol.self, name: "home"),
                downloadVideosUseCase: resolve(),
                assetLoader: resolve()
            )
        }
        .implements(HomeViewModelProtocol.self)
        .scope(.graph)
        
        register {
            VideoDiskReleaseMaster()
        }
        .implements(DiskReleaseMasterProtocol.self)
        .scope(.shared)
    }
}

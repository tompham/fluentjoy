//
//  AppError.swift
//  Fluentjoy
//
//  Created by Tuan Pham on 09/01/2024.
//

import Foundation

enum AppError: LocalizedError {
    
    case network(error: Error)
    
    case apiError(error: APIError)
    
    case unAuthorized
    
    case internalServerError
    
    case invalidRefreshToken
    
    case underlyingError(error: Error)
    
    case unknown
}

extension AppError {
    var message: String {
       return ""
    }
}

extension AppError: Equatable {
    static func == (lhs: AppError, rhs: AppError) -> Bool {
        switch (lhs, rhs) {
        case (.network(let lhsError), .network(let rhsError)):
            return lhsError.localizedDescription == rhsError.localizedDescription
            
        case (.apiError(let lhsError), .apiError(let rhsError)):
            return lhsError == rhsError
            
        case (.unAuthorized, .unAuthorized):
            return true
            
        case (.internalServerError, .internalServerError):
            return true
            
        case (.invalidRefreshToken, .invalidRefreshToken):
            return true
        
        case (.underlyingError(let lhsError), .underlyingError(let rhsError)):
            return lhsError.localizedDescription == rhsError.localizedDescription
            
        case (.unknown, .unknown):
            return true
            
        default:
            return false
        }
    }
}

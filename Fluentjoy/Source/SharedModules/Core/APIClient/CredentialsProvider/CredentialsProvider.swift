//
//  CredentialsProvider.swift
//  Fluentjoy
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

final class CredentialsProvider {
    
    static let `default` = CredentialsProvider()
}

// MARK: - AccessTokenProviderProtocol\

extension CredentialsProvider: CredentialsProviderProtocol {
    
    func getAccessToken() -> String? {
        return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNzEzMjA3NzMxLCJpYXQiOjE3MTI2MDI5MzEsImp0aSI6IjY0OTQ2MTc0MjcwNjQxOWFhZjdmZjFhM2Q2YzA2MzlhIiwidXNlcl9pZCI6MjEzNjc5fQ.zDh0tY18T8dv5W5CcGPRoPGpctJemZDUkrd8AQwwK0c"
    }
}

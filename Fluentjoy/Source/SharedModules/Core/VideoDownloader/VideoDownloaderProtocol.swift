//
//  VideoDownloaderProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 12/04/2024.
//

import Foundation
import RxSwift
import Moya

protocol VideoDownloaderProtocol {
    
    func download(_ urls: [String]) -> Observable<[String]>
    
    func download(_ url: String) -> Observable<String>
}


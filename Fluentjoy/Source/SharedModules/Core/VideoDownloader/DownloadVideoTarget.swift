//
//  DownloadVideoTarget.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 12/04/2024.
//

import Foundation
import Moya

struct DownloadVideoTarget: TargetType {
    
    let url: String
    
    var baseURL: URL {
        guard let url = URL(string: url) else {
            fatalError("Invalid url")
        }
        
        return url
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var path: String {
        return .empty
    }
    
    var task: Task {
        return .downloadDestination(downloadDestination)
    }
    
    var downloadDestination: DownloadDestination {
        return { tempURL, response in
            return (documentDirectoryURL, [.removePreviousFile, .createIntermediateDirectories])
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    private var cachedGroup: String {
        return "fluentjoy"
    }
    
    var documentDirectoryURL: URL {
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentURL.appendingPathComponent("\(cachedGroup)/\(url)")
        return fileURL
    }
}

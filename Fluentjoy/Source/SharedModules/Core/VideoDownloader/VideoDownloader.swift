//
//  VideoDownloader.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 12/04/2024.
//

import Foundation
import RxSwift
import Moya

final class VideoDownloader {
    
    private let apiClient: APIClientProtocol
    
    init(apiClient: APIClientProtocol) {
        self.apiClient = apiClient
    }
}

extension VideoDownloader: VideoDownloaderProtocol {
    
    func download(_ urls: [String]) -> Observable<[String]> {
        return Observable.zip(urls.map { download($0) })
    }
    
    func download(_ url: String) -> Observable<String> {
        let target = DownloadVideoTarget(url: url)
        return apiClient.request(target: target)
            .asObservable()
            .map { _ -> String in
                return target.documentDirectoryURL.absoluteString
            }
    }
}

//
//  DiskReleaseMasterProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 12/04/2024.
//

import Foundation
import RxSwift

protocol DiskReleaseMasterProtocol {
    
    func release(atPath path: String)
}

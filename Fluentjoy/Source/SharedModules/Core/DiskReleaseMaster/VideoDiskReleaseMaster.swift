//
//  VideoDiskReleaseMaster.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 12/04/2024.
//

import Foundation
import RxSwift

final class VideoDiskReleaseMaster {
    
    
    private var cachedGroup: String {
        return "fluentjoy"
    }
    
    private var documentDirectoryURL: URL {
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentURL.appendingPathComponent("\(cachedGroup)")
        return fileURL
    }
}

extension VideoDiskReleaseMaster: DiskReleaseMasterProtocol {
    
    func release(atPath path: String) {
//        do {
//            let contents = try FileManager.default.contents(atPath: path)
//            for file in contents {
//                if FileManager.default.fileExists(atPath: <#T##String#>)
//                try FileManager.default.removeItem(at: file)
//            }
//        } catch {
//            print(error)
//        }
    }
}


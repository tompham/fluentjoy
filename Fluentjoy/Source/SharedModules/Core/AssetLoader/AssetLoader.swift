//
//  AssetLoader.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 13/04/2024.
//

import Foundation
import RxSwift
import AVFoundation

final class AssetLoader {}

extension AssetLoader: AssetLoaderProtocol {
    
    func load(assetURL: String) -> Observable<AVAsset> {
        return Observable.create { observer in
            
            if let url = URL(string: assetURL) {
                let asset = AVAsset(url: url)
                asset.loadValuesAsynchronously(forKeys: ["playable"]) {
                    var error: NSError? = nil
                    let status = asset.statusOfValue(forKey: "playable", error: &error)
                    switch status {
                    case .loaded:
                        observer.onNext(asset)
                        observer.onCompleted()
                        
                    case .failed:
                        observer.onError(AssetLoaderError.loadAssetFailed)
                        
                    case .cancelled:
                        observer.onNext(asset)
                        observer.onCompleted()
                        
                    default:
                        observer.onCompleted()
                    }
                }
            } else {
                observer.onError(AssetLoaderError.invalidateURL)
            }
            
            return Disposables.create()
        }
    }
}

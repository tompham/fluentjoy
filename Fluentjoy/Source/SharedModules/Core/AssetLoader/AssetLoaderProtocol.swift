//
//  AssetLoaderProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 13/04/2024.
//

import Foundation
import AVFoundation
import RxSwift

protocol AssetLoaderProtocol {
    
    func load(assetURL: String) -> Observable<AVAsset>
}

//
//  AssetLoaderError.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 13/04/2024.
//

import Foundation

enum AssetLoaderError: Error {
    
    case invalidateURL
    case loadAssetFailed
}

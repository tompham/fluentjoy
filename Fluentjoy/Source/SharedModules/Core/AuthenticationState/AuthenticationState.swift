//
//  AuthenticationState.swift
//  Fluentjoy
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

final class AuthenticationState {
    
    static let `shared` = AuthenticationState(
        credentialsProvider: CredentialsProvider.default
    )
    
    // MARK: - Dependencies
    
    private let credentialsProvider: CredentialsProviderProtocol
    
    // MARK: - Initializers

    init(credentialsProvider: CredentialsProviderProtocol) {
        self.credentialsProvider = credentialsProvider
    }
    
    /// Returns the authentication state.
    /// 
    /// - Note: This property is used to check the authentication state.
    var isAuthenticated: Bool {
        
        if let token = credentialsProvider.getAccessToken(), token.isNotEmpty {
            return true
        }
        
        return false
    }
}

//
//  HomeViewModel.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation
import RxSwift
import RxRelay

final class HomeViewModel {
    
    // MARK: - Data
    
    private var page = Config.initialPageNumber
    private let videoItemsRelay = BehaviorRelay<[VideoDataResponse]>(value: [])
    private let isoScheduler = ConcurrentDispatchQueueScheduler(qos: .default)
    private let isAvailableToLoadMoreRelay = BehaviorRelay(value: true)
    private let downloadingItemsRelay = PublishRelay<[VideoDataResponse]>()
    private let addingVideoModelRelay = PublishRelay<PostVideoNodeModel>()
    
    // MARK: - Inputs
    
    let loadMoreTrigger = PublishRelay<Void>()
    
    // MARK: - Dependencies
    
    private let getVideosUseCase: GetVideosUseCaseProtocol
    private let downloadVideosUseCase: DownloadVideosUseCaseProtocol
    private let assetLoader: AssetLoaderProtocol
    
    private let disposebag = DisposeBag()
    
    // MARK: - Initializers
    
    init(
        getVideosUseCase: GetVideosUseCaseProtocol,
        downloadVideosUseCase: DownloadVideosUseCaseProtocol,
        assetLoader: AssetLoaderProtocol
    ) {
        self.getVideosUseCase = getVideosUseCase
        self.downloadVideosUseCase = downloadVideosUseCase
        self.assetLoader = assetLoader
        
        bindingData()
    }
    
    private func bindingData() {
        loadMoreTrigger
            .observe(on: isoScheduler)
            .subscribe(onNext: { [weak self] in
                guard let self, self.isAvailableToLoadMoreRelay.value else { return }
                self.page += 1
                self.loadVideos(page: page)
            })
            .disposed(by: disposebag)
        
        downloadingItemsRelay
            .do(onNext: { [weak self] items in
                guard let self else { return }
                let newItems = self.videoItemsRelay.value + items
                self.videoItemsRelay.accept(newItems)
            })
            .concatMap { items -> Observable<PostVideoNodeModel> in
                return Observable.from(items)
                    .flatMap { [weak self] item -> Observable<PostVideoNodeModel> in
                        guard let self, let url = item.appVideoUrl, url.isNotEmpty else {
                            return .empty()
                        }
                        
                        return self.assetLoader.load(assetURL: url)
                            .map { PostVideoNodeModel(entity: item, asset: $0) }
                    }
            }
            .subscribe(onNext: { model in
                self.addingVideoModelRelay.accept(model)
            })
            .disposed(by: disposebag)
    }
}

extension HomeViewModel: HomeViewModelInputProtocol {
    
    func fetchData() {
        loadVideos(page: page)
    }
    
    private func loadVideos(page: Int) {
        getVideosUseCase.execute(page: page)
            .observe(on: isoScheduler)
            .map { $0.data.or([]) }
            .do(onNext: { [weak self] items in
                guard let self else { return }
                self.isAvailableToLoadMoreRelay.accept(items.isNotEmpty)
            })
            .subscribe(on: isoScheduler)
            .subscribe(onNext: { [weak self] items in
                guard let self else { return }
                self.downloadingItemsRelay.accept(items)
            })
            .disposed(by: disposebag)
    }
}

// MARK: - HomeViewModelOutputProtocol

extension HomeViewModel: HomeViewModelOutputProtocol {
    
    var addingNewVideoModel: Observable<PostVideoNodeModel> {
        return addingVideoModelRelay.asObservable()
    }
    
    var videoViewModels: Observable<[PostVideoNodeModel]> {
        return videoItemsRelay.skip(1)
            .distinctUntilChanged()
            .map { items -> [PostVideoNodeModel] in
                return items.map { PostVideoNodeModel(entity: $0) }
            }
            .asObservable()
    }
}

// MARK: - HomeViewModelProtocol

extension HomeViewModel: HomeViewModelProtocol {
    
    var input: HomeViewModelInputProtocol {
        return self
    }
    
    var output: HomeViewModelOutputProtocol {
        return self
    }
}

// MARK: - HomeViewModel

extension HomeViewModel {
    
    enum Config {
        static let initialPageNumber = 1
    }
}

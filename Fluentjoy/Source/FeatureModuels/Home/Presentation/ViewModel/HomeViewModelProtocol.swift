//
//  HomeViewModelProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation
import RxSwift
import RxRelay

protocol HomeViewModelProtocol {
    
    var input: HomeViewModelInputProtocol { get }
    var output: HomeViewModelOutputProtocol { get }
}

protocol HomeViewModelInputProtocol {
    
    func fetchData()
    
    var loadMoreTrigger: PublishRelay<Void> { get }
}

protocol HomeViewModelOutputProtocol {
    
    var addingNewVideoModel: Observable<PostVideoNodeModel> { get }
    var videoViewModels: Observable<[PostVideoNodeModel]> { get }
}

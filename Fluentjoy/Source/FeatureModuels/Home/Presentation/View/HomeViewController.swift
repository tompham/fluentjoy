//
//  HomeViewController.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import UIKit
import RxCocoa
import RxSwift
import AsyncDisplayKit

class HomeViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - UI
    
    private lazy var tableNode = makeTableNode()
    private lazy var loadingView = makeLoadingView()
    
    // MARK: - Data
    
    private var videoViewModels: [PostVideoNodeModel] = []
    private var curretnActiveNode: PostVideoNode?
    private var cachedNodes: [IndexPath: PostVideoNode] = [:]
    private var focusingItemIndex: Int?
    
    // MARK: - Dependencies
    
    private lazy var statePresenter = makeStatePresenter()
    private let viewModel: HomeViewModelProtocol
    private let disposeBag = DisposeBag()
    
    // MARK: - Initializers
    
    init(viewModel: HomeViewModelProtocol) {
        
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bindingData()
        viewModel.input.fetchData()
        statePresenter.startLoading(animated: false, completionHandler: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        tableNode.view.frame = view.bounds
    }
}

// MARK: - Views Settings

extension HomeViewController {
    
    private func setupViews() {
        view.addSubview(tableNode.view)
    }
    
    func bindingData() {
        
        viewModel.output.addingNewVideoModel
            .asDriver(onErrorDriveWith: .never())
            .drive(onNext: { [weak self] newVideo in
                guard let self else { return }
                self.insertNewRowInTableNode(newVideo: newVideo)
                self.statePresenter.endLoading(hasError: false, hasContent: true, completionHandler: nil)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.addingNewVideoModel
            .take(1)
            .asDriver(onErrorDriveWith: .never())
            .delay(.milliseconds(250))
            .drive(onNext: { [weak self] _ in
                guard let self else { return }
                self.statePresenter.endLoading(hasError: false, hasContent: true, completionHandler: nil)
                self.makeTheFirstVideoPlayIfNeeded()
            })
            .disposed(by: disposeBag)
    }
    
    private func makeTheFirstVideoPlayIfNeeded() {
        guard let firstNode = cachedNodes[.zero] else { return }
        firstNode.play()
    }
    
    private func insertNewRowInTableNode(newVideo: PostVideoNodeModel) {
        let indexPath = IndexPath(row: videoViewModels.count, section: .zero)
        videoViewModels.append(newVideo)
        tableNode.insertRows(at: [indexPath], with: .none)
    }
}

// MARK: - ASTableDataSource

extension HomeViewController: ASTableDataSource {
    
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return 1
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return videoViewModels.count
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeForRowAt indexPath: IndexPath) -> ASCellNode {
        
        if let existedNode = cachedNodes[indexPath] {
            return existedNode
        }
        
        let node = PostVideoNode()
        node.backgroundColor = .black
        node.configure(viewModel: videoViewModels[indexPath.row])
        cachedNodes[indexPath] = node
        return node
    }
}

// MARK: - ASTableDelegate

extension HomeViewController: ASTableDelegate {
    
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let width = UIScreen.main.bounds.size.width
        let min = CGSize(width: width, height: UIScreen.main.bounds.size.height)
        let max = CGSize(width: width, height: .infinity)
        return ASSizeRangeMake(min, max)
    }
    
    func shouldBatchFetch(for tableNode: ASTableNode) -> Bool {
        return true
    }
    
    func tableNode(
        _ tableNode: ASTableNode,
        willBeginBatchFetchWith context: ASBatchContext
    ) {
        viewModel.input.loadMoreTrigger.accept(())
        context.completeBatchFetching(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentYOffset = scrollView.contentOffset.y
        let itemRow = Int(contentYOffset / UIScreen.main.bounds.height)
        
        if itemRow == focusingItemIndex {
            return
        }
        
        if let index = focusingItemIndex,
           let previousNode = cachedNodes[.init(row: index, section: .zero)] {
            previousNode.pause()
        }
        
        if let goingToPlayNode = cachedNodes[.init(row: itemRow, section: .zero)] {
            goingToPlayNode.play()
        }
        
        focusingItemIndex = itemRow
    }
}

// MARK: - Privates

extension HomeViewController {
    
    private func makeTableNode() -> ASTableNode {
        let node = ASTableNode()
        node.view.backgroundColor = .black
        node.delegate = self
        node.dataSource = self
        node.view.isPagingEnabled = true
        node.view.separatorStyle = .singleLine
        node.leadingScreensForBatching = 2.0
        return node
    }
    
    private func makeStatePresenter() -> DSStatePresentable {
        
        return DSStatePresenter(
            stateContainerView: view,
            loadingStateView: loadingView
        )
    }
    
    private func makeLoadingView() -> UIView {
        let view = Bundle.loadView(fromNib: "HomeLoadingView", withType: HomeLoadingView.self)
        return view
    }
}

extension IndexPath {
    static let zero = IndexPath(row: .zero, section: .zero)
}

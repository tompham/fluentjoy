//
//  HomeLoadingView.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 11/04/2024.
//

import UIKit
import SkeletonView

class HomeLoadingView: UIView {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        showAnimatedGradientSkeleton()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        showAnimatedGradientSkeleton()
        
    }
}

//
//  PostVideoNodeModel.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 11/04/2024.
//

import Foundation
import AVFoundation 

struct PostVideoNodeModel: Equatable {
    
    let entity: VideoDataResponse
    let asset: AVAsset?
    
    init(entity: VideoDataResponse, asset: AVAsset? = nil) {
        self.entity = entity
        self.asset = asset
    }
}

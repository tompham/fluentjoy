//
//  PostVideoNode.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 11/04/2024.
//

import Foundation
import AsyncDisplayKit

final class PostVideoNode: ASCellNode {

    lazy var videoNode = makeVideoNode()

    override init() {
        super.init()
        self.selectionStyle = .none
        self.automaticallyManagesSubnodes = true
    }
    
    func configure(viewModel: PostVideoNodeModel) {
        
        addSubnode(videoNode)
        videoNode.configure(viewModel: viewModel)
    }
    
    override func layoutSpecThatFits(
        _ constrainedSize: ASSizeRange
    ) -> ASLayoutSpec {
        return ASInsetLayoutSpec(
            insets: .zero,
            child: videoNode
        )
    }
    
    func pause() {
        DispatchQueue.main.async {
            self.videoNode.videoNode.pauseVideo()
        }
    }
    
    func play() {
        DispatchQueue.main.async {
            self.videoNode.videoNode.playVideo(forcePlay: true)
        }
    }
}

extension PostVideoNode {
    
    private func makeVideoNode() -> VideoContentNode {
        let node = VideoContentNode()
        node.backgroundColor = .black
        return node
    }
}

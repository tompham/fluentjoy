//
//  ASDisplayNode.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 11/04/2024.
//

import Foundation
import AsyncDisplayKit

class VideoNode: ASDisplayNode {
    
    // MARK: - Properties
    
    private var state: VideoState?
    private let ratio: CGFloat
    private let automaticallyPause: Bool
    private let videoGravity: AVLayerVideoGravity
    private var willCache: Bool = true
    private var playControlNode: ASDisplayNode?
    
    // MARK: - UI
    
    private lazy var videoNode = makeVideoNode()
    private lazy var loadingIndicatorNode = makeLoadingIndicator()
    
    // MARK: - Initializers
    
    required init(
        ratio: CGFloat,
        videoGravity: AVLayerVideoGravity,
        automaticallyPause: Bool = true,
        playControlNode: ASDisplayNode?
    ) {
        self.ratio = ratio
        self.videoGravity = videoGravity
        self.automaticallyPause = automaticallyPause
        self.playControlNode = playControlNode
        super.init()
        self.automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(
        _ constrainedSize: ASSizeRange
    ) -> ASLayoutSpec {
        
        let loadingSpec = ASRelativeLayoutSpec(
            horizontalPosition: .center,
            verticalPosition: .center,
            sizingOption: .minimumSize,
            child: loadingIndicatorNode
        )
        let videoSpec = ASRatioLayoutSpec(
            ratio: ratio,
            child: videoNode
        )
        return ASOverlayLayoutSpec(
            child: videoSpec,
            overlay: loadingSpec
        )
    }
    
    func setPlayControlNode(_ node: ASDisplayNode) {
        self.playControlNode = node
    }
    
    func setVideoAsset(_ asset: AVAsset, isCache: Bool = true) {
        willCache = isCache
        state = .readyToPlay
        DispatchQueue.main.async {
            self.videoNode.asset = asset
        }
    }
}

// MARK: - Video ControlEvent

extension VideoNode {
    
    func replayVideo() {
        guard let state, case .pause = state else { return }
        self.state = .readyToPlay
        playVideo(forcePlay: true)
    }
    
    func playVideo(forcePlay: Bool = false) {
        guard let state else { return }
        
        switch state {
        case .pause, .readyToPlay:
            videoNode.play()
            videoNode.playerLayer?.videoGravity = videoGravity
            playControlNode?.isHidden = true
            self.state = .play
            
        default:
            break
        }
    }
    
    func pauseVideo() {
        guard let state, state == .play else { return }
        videoNode.pause()
        videoNode.asset?.cancelLoading()
        playControlNode?.isHidden = false
        if !willCache {
            videoNode.asset = nil
        }
        self.state = .pause
    }
}

// MARK: - ASVideoNodeDelegate

extension VideoNode: ASVideoNodeDelegate {
    
    func videoNode(_ videoNode: ASVideoNode, willChange state: ASVideoNodePlayerState, to toState: ASVideoNodePlayerState) {
        switch toState {
        case .unknown, .initialLoading, .loading:
            loadingIndicatorNode.isHidden = false
        default:
            loadingIndicatorNode.isHidden = true
        }
    }
}

// MARK: - Privates

extension VideoNode {
    
    private func makeVideoNode() -> ASVideoNode {
        let node = ASVideoNode()
        node.shouldAutoplay = false
        node.shouldAutorepeat = false
        node.delegate = self
        return node
    }
    
    private func makeLoadingIndicator() -> ASDisplayNode {
        let node = ASDisplayNode {
            let indicator = UIActivityIndicatorView(style: .large)
            indicator.tintColor = .white
            indicator.clipsToBounds = true
            indicator.startAnimating()
            return indicator
        }
        node.style.preferredSize = .init(width: 88, height: 88)
        return node
    }
    
}

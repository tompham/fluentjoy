//
//  VideoState.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 11/04/2024.
//

import Foundation

enum VideoState {
    case readyToPlay
    case play
    case pause
}

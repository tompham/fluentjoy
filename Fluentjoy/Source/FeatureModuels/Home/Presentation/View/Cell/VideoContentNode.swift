//
//  VideoContentNode.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 11/04/2024.
//

import Foundation
import AsyncDisplayKit

class VideoContentNode: ASDisplayNode {
    
    var state: VideoState?
    
    lazy var playButton = makePlayButton()
    lazy var videoNode = makeVideoNode()
    
    override init() {
        super.init()
        self.backgroundColor = .white
        self.automaticallyManagesSubnodes = true
    }
    
    @objc func replay() {
        self.videoNode.replayVideo()
    }
}

extension VideoContentNode {
    
    func configure(viewModel: PostVideoNodeModel) {
        if let asset = viewModel.asset {
            self.videoNode.setVideoAsset(asset, isCache: true)
        }
    }
}

extension VideoContentNode {
    
    func videoRatioLayout() -> ASLayoutSpec {
        let ratio = UIScreen.main.bounds.height / UIScreen.main.bounds.width
        let videoRatioLayout = ASRatioLayoutSpec(
            ratio: ratio,
            child: videoNode
        )
        let playButtonCenterLayout = ASCenterLayoutSpec(
            centeringOptions: .XY,
            sizingOptions: [],
            child: playButton
        )
        return ASOverlayLayoutSpec(
            child: videoRatioLayout,
            overlay: playButtonCenterLayout
        )
    }
    
    override func layoutSpecThatFits(
        _ constrainedSize: ASSizeRange
    ) -> ASLayoutSpec {
        let stackLayoutSpec = ASStackLayoutSpec(
            direction: .vertical,
            spacing: .zero,
            justifyContent: .start,
            alignItems: .stretch,
            children: [videoRatioLayout()]
        )
        return ASInsetLayoutSpec(
            insets: .zero,
            child: stackLayoutSpec
        )
    }
}

extension VideoContentNode {
    
    private func makeVideoNode() -> VideoNode {
        let ratio = UIScreen.main.bounds.height / UIScreen.main.bounds.width
        let node = VideoNode(
            ratio: ratio,
            videoGravity: .resizeAspect,
            playControlNode: playButton
        )
        node.backgroundColor = UIColor.black.withAlphaComponent(0.05)
        return node
    }
    
    private func makePlayButton() -> ASButtonNode {
        let node = ASButtonNode()
        node.contentMode = .scaleAspectFill
        node.clipsToBounds = true
        node.style.preferredSize = .init(width: 64, height: 64)
        node.addTarget(self, action: #selector(replay), forControlEvents: .touchUpInside)
        return node
    }
}

//
//  VideoDataObject.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 13/04/2024.
//

import Foundation
import RealmSwift

final class VideoDataObject: Object {
    
    @Persisted var appVideoUrl: String?
}

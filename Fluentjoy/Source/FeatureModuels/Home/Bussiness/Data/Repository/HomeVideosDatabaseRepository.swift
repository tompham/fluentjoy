//
//  HomeVideosDatabaseRepository.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 13/04/2024.
//

import Foundation
import RxSwift
import RealmSwift

final class HomeLocalVideosDataRepository {
    
    
    private let realmManager: RealmManagerType
    private let disposeBag = DisposeBag()
    
    init(realmManager: RealmManagerType = RealmManager.default) {
        self.realmManager = realmManager
    }
}

extension HomeLocalVideosDataRepository: LocalVideosDataRepositoryProtocol {
    
    func write(page: Int, data: PageResponse) {
        DispatchQueue.main.async {
            
            let object = PageObject()
            object.page = page
            object.count = data.count
            object.pageSize = data.pageSize
            let videos = List<VideoDataObject>()
            data.data?.forEach { video in
                let videoObject = VideoDataObject()
                videoObject.appVideoUrl = video.appVideoUrl
                videos.append(videoObject)
            }
            
            object.data = videos
            self.realmManager.create(object: object, update: true, writeBlock: { _ in })
                .subscribe()
                .disposed(by: self.disposeBag)

        }
    }
    
    func read(page: Int) -> Observable<PageResponse> {
        
        guard let pageObject = realmManager.get(
            PageObject.self,
            filter: nil
        ).first(where: { $0.page == page }) else {
            return .never()
        }
        
        let videos = pageObject.data.map {
            VideoDataResponse(
                appVideoUrl: $0.appVideoUrl
            )
        }
        let response = PageResponse(
            count: pageObject.count,
            pageSize: pageObject.pageSize,
            data: Array(
                videos
            )
        )
        
        return .just(response)
    }
    
}

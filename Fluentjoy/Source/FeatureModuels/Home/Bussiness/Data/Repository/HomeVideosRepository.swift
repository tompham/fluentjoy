//
//  HomeVideosRepository.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation
import Moya
import RxSwift

final class HomeVideosRepository {
    
    private let apiClient: APIClientProtocol
    private let decoder: DecoderProtocol
    
    init(apiClient: APIClientProtocol, decoder: DecoderProtocol) {
        self.apiClient = apiClient
        self.decoder = decoder
    }
}

extension HomeVideosRepository: VideosRepositoryProtocol {
    
    func fetchVideos(page: Int) -> Observable<PageResponse> {
        let target = HomeVideosTarget(page: page)
        return apiClient.request(target: target)
            .asObservable()
            .decode(decoder: decoder)
    }
}

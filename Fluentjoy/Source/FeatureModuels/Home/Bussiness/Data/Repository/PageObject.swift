//
//  PageObject.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 13/04/2024.
//

import Foundation
import RealmSwift

final class PageObject: Object {
    @Persisted var count: Int?
    @Persisted var pageSize: Int?
    @Persisted var data: List<VideoDataObject>
    @Persisted var page: Int
    
    override class func primaryKey() -> String? {
        return "page"
    }
}

//
//  HomeVideosTarget.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation
import Moya

struct HomeVideosTarget: AppTargetType, AccessTokenAuthorizable {
    
    let page: Int
    
    init(page: Int) {
        self.page = page
    }
    
    var path: String {
        return "/home_video/"
    }
    
    var task: Task {
        let parameters = [
            "page": page
        ]
        return .requestParameters(
            parameters: parameters,
            encoding: URLEncoding.queryString
        )
    }
    
    var authorizationType: AuthorizationType? {
        return .bearer
    }

    
    var method: Moya.Method {
        return .get
    }
}

//
//  AppTargetType.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation
import Moya

protocol AppTargetType: TargetType {}

extension AppTargetType {
    
    var baseURL: URL {
        guard let url = URL(string: "https://api.fluentjoy.com/api/v1/cms") else {
            fatalError("invalidated base URL")
        }
        
        return url
    }
    
    var headers: [String : String]? {
        return [:]
    }
}

//
//  DownloadVideosUseCaseProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 12/04/2024.
//

import Foundation
import RxSwift

protocol DownloadVideosUseCaseProtocol {
    
    func execute(_ urls: [String]) -> Observable<[String]>
    
    func execute(_ url: String) -> Observable<String>
}

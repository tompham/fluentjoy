//
//  GetHomeVideosUseCase.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation
import RxSwift

final class GetHomeVideosUseCase {
    
    private let remoteRepository: VideosRepositoryProtocol
    private let localRepository: LocalVideosDataRepositoryProtocol
    private let isoScheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    private let disposeBag = DisposeBag()
    
    init(
        remoteRepository: VideosRepositoryProtocol,
        localRepository: LocalVideosDataRepositoryProtocol
    ) {
        self.remoteRepository = remoteRepository
        self.localRepository = localRepository
    }
}

extension GetHomeVideosUseCase: GetVideosUseCaseProtocol {
    
    func execute(page: Int) -> Observable<PageResponse> {
        return Observable.merge(
            localRepository.read(page: page),
            remoteRepository.fetchVideos(page: page)
                .do(
                    onNext: { pageResponse in
                        self.localRepository.write(
                            page: page,
                            data: pageResponse
                        )
                    }
                )
        )
        .subscribe(on: isoScheduler)
    }
}

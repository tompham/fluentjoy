//
//  DownloadVideosUseCase.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 12/04/2024.
//

import Foundation
import RxSwift

final class DownloadVideosUseCase {
    
    private let downloader: VideoDownloaderProtocol
    
    init(downloader: VideoDownloaderProtocol) {
        self.downloader = downloader
    }
}

extension DownloadVideosUseCase: DownloadVideosUseCaseProtocol {
    
    func execute(_ urls: [String]) -> Observable<[String]> {
        return downloader.download(urls)
    }
    
    func execute(_ url: String) -> Observable<String> {
        return downloader.download(url)
    }
}

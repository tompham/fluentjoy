//
//  GetVideosUseCaseProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation
import RxSwift

protocol GetVideosUseCaseProtocol {
    
    func execute(page: Int) -> Observable<PageResponse>
}

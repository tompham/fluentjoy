//
//  VideosRepositoryProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation
import RxSwift

protocol VideosRepositoryProtocol {
    
    func fetchVideos(page: Int) -> Observable<PageResponse>
}

//
//  LocalVideosDataWriterProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 13/04/2024.
//

import Foundation

protocol LocalVideosDataWriterProtocol {
    
    func write(page: Int, data: PageResponse)
}

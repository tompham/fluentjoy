//
//  LocalVideosDataReaderProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 13/04/2024.
//

import Foundation
import RxSwift

protocol LocalVideosDataReaderProtocol {
    
    func read(page: Int) -> Observable<PageResponse>
}

//
//  LocalVideosDataRepositoryProtocol.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 13/04/2024.
//

import Foundation

protocol LocalVideosDataRepositoryProtocol: LocalVideosDataReaderProtocol, LocalVideosDataWriterProtocol {}

//
//  PageModel.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation

struct PageResponse{
    let count: Int?
    let pageSize: Int?
    let data: [VideoDataResponse]?
}

extension PageResponse: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case count, data
        case pageSize = "page_size"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        count = container.decodeIfPossible(key: .count)
        pageSize = container.decodeIfPossible(key: .pageSize)
        data = container.decodeIfPossible(key: .data)
    }
}

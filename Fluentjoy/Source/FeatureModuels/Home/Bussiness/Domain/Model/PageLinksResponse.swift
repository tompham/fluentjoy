//
//  PageLinksResponse.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation

struct PageLinksResponse {
    let next: String?
    let previous: String?
}

extension PageLinksResponse: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case next, previous
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        next = container.decodeIfPossible(key: .next)
        previous = container.decodeIfPossible(key: .previous)
    }
}

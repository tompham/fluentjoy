//
//  VideoDataResponse.swift
//  Fluentjoy
//
//  Created by Trần Minh Lý on 10/04/2024.
//

import Foundation

struct VideoDataResponse {
    
    let appVideoUrl: String?
}

extension VideoDataResponse: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case appVideoUrl = "app_video_url"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        appVideoUrl = container.decodeIfPossible(key: .appVideoUrl).orEmpty
            .replacingOccurrences(
                of: "https://fluentjoy-production.s3.amazonaws.com",
                with: "https://d3sdyi4dt2a4dk.cloudfront.net"
            )
    }
}

extension VideoDataResponse: Equatable {}
